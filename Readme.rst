=========
PixelGrid
=========

A grid generator that creates a grid with text that can be used for dom/canvas
based animation. A very simple tool to help with visualizing your animations to
see if your math was correct.

I originally wrote this as a python cli script but there's no need for
distribution if the browser can do the work and you don't have to hassle with
pip install -r requirements.txt...

This is the standalone/static version for distribution.

&copy; John Boisselle 2013 <ping_me@johnboisselle.com>

Please view the license in file: LICENSE

