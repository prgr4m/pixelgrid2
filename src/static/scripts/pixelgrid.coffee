# pixelgrid.coffee
info_content =
	about: """
		<p>
			<strong>PixelGrid<sup>2</sup></strong> is a grid generator useful for
			visualizing in-browser DOM/Canvas animations.
		</p>
		<p>
			I originally wrote PixelGrid as a cli python script with a dependency on
			PIL. I wanted to generate a grid to test out dom based animations to see
			if my math was working correctly without having to open up a graphics
			program to generate one. Since the web browser can perform the same
			capabilities without a user having a native python installation with PIL,
			I decided to write this trivial app.
		</p>
		<p>
			When the grid is generated, be sure to "Right click" and "Save as..."
		</p>
		<p>
			<strong>Requirements:</strong><br/>
			A modern web browser that supports canvas and new form elements.
			Javascript also must be enabled.
		</p>
		"""
	grid_width: """
		<h4>Grid Width</h4>
		<p>
			The grid length/width to be generated. Minimum is set to 100 pixels.
			Maximum grid width is not defined. Input must be numeric and will be
			parsed as an integer. Value must be in increments of 10.
		</p>
		<p>
			Default: 100
		</p>
		"""
	grid_height: """
		<h4>Grid Height</h4>
		<p>
			The height of the grid to be generated. Minimum is set to 100 pixels.
			Maximum grid height is not defined. Input must be numeric and will be
			parsed as an integer. Value must be in increments of 10.
		</p>
		<p>
			Default: 100
		</p>
		"""
	grid_line_color: """
		<h4>Line Color</h4>
		<p>
			The color of the rendered lines of the grid. Input is in hex color format.
		</p>
		<p>
			Default: #586E75
		</p>
		"""
	grid_bg_color: """
		<h4>Background Color</h4>
		<p>
			The color of the background fill of the grid. Input is in hex color format.
		</p>
		<p>
			Default: #FDF6E3
		</p>
		"""
	grid_transparency: """
		<h4>Transparency Level</h4>
		<p>
			Changes the alpha transparency of the image as a whole. Helpful if you
			want to see something underneath the grid inside of the browser. Maximum
			value is 1 making the grid generated being fully opaque. Minumum value is
			0 rendering the image completely transparent. Input will be parsed as a
			float.
		</p>
		<p>
			Default: 1
		</p>
		"""
user_error_message = ""

class PixelGrid
	constructor: (@_width, @_height, @_line_color, @_bg_color, @_transparency, @_context)->
		@_x_coords = (x for x in [0..@_width] by 10)
		@_y_coords = (y for y in [0..@_height] by 10)
	_draw_grid: ->
		# need to fill in the background with the transparency...
		if @_transparency isnt 1
			# convert to rgba and create the color
			red = parseInt "0x#{@_bg_color[1..2]}"
			green = parseInt "0x#{@_bg_color[3..4]}"
			blue = parseInt "0x#{@_bg_color[5..6]}"
			@_context.fillStyle = "rgba(#{red}, #{green}, #{blue}, #{@_transparency})"
		else
			@_context.fillStyle = @_bg_color
		@_context.fillRect 0, 0, @_width, @_height
		
		#prepping for drawing lines
		@_context.strokeStyle = @_line_color
		@_context.lineWidth = 1
		@_context.beginPath()
		
		# drawing vertical lines
		for x in @_x_coords
			@_context.moveTo x, 0
			@_context.lineTo x, @_height
		
		# drawing horizontal lines
		for y in @_y_coords
			@_context.moveTo 0, y
			@_context.lineTo @_width, y
		@_context.stroke()
		
		# drawing dark markers
		@_context.strokeStyle = "#000000"
		@_context.beginPath()
		for x in @_x_coords
			if x % 100 is 0
				@_context.moveTo x, 0
				@_context.lineTo x, @_height
		for y in @_y_coords
			if y % 100 is 0
				@_context.moveTo 0, y
				@_context.lineTo @_width, y
		@_context.stroke()
		
	_draw_text: ->
		@_context.fillStyle = "#000000"
		@_context.lineWidth = 1
		@_context.font = "10px sans-serif"
		
		# drawing x labels
		for x in @_x_coords
			if x % 100 is 0 and x isnt 0
				label_x = "#{x} px"
				if label_x.length > 6
					@_context.fillText label_x, x - 40, 97
				else
					@_context.fillText label_x, x - 35, 97
		# drawing y labels
		for y in @_y_coords
			if y % 100 is 0 and y isnt 0
				label_y = "#{y} px"
				if label_y.length > 6
					@_context.fillText label_y, 60, y - 3
				else
					@_context.fillText label_y, 65, y - 3
	
	render: ->
		@_draw_grid()
		@_draw_text()
	
	get_image: ->
		canvas = document.getElementById "GridSurface"
		window.open canvas.toDataURL(),
			"Right Click; Save As",
			"width=#{canvas.width},height=#{canvas.height},toolbar=0,resizable=0"
	

get_input_values = ->
	values = []
	controls = [
		"#GridWidth"
		"#GridHeight"
		"#GridLineColor"
		"#GridBgColor"
		"#GridTransparency"
	]
	for control in controls
		values.push $(control).val()
	values

all_input_okay = ->
	all_vars = [
		width
		height
		line_color
		bg_color
		transparency
	] = get_input_values()
	
	hex_pattern = /^\#[0-9a-fA-F]{3,6}$/
	
	if parseInt(width) is NaN
		user_error_message = "Grid Width needs to be a number"
		$('#GridWidth').focus()
		return off
	else if parseInt(width) % 10 isnt 0
		user_error_message = "Grid Width needs to be in increments of 10"
		$('#GridWidth').focus()
		return off
	else if parseInt(width) <= 100
		user_error_message = "Grid Width needs to be greater than or equal to 100"
		$('#GridWidth').focus()
		return off
	
	if parseInt(height) is NaN
		user_error_message = "Grid Height needs to be a number"
		$('#GridHeight').focus()
		return off
	else if parseInt(height) % 10 isnt 0
		user_error_message = "Grid Height needs to be in increments of 10"
		$('#GridHeight').focus()
		return off
	else if parseInt(height) <= 100
		user_error_message = "Grid Height needs to be greater than or equal to 100"
		$('#GridHeight').focus()
		return off
	
	if hex_pattern.test(line_color) is off
		user_error_message = "Line Color needs to be in hex color format"
		$('#GridLineColor').focus()
		return off
	else if line_color.length isnt 7
		user_error_message = "Line Color needs to be in long hand format"
		$('#GridLineColor').focus()
		return off
	
	if hex_pattern.test(bg_color) is off
		user_error_message = "Background Color needs to be in hex color format"
		$('#GridBgColor').focus()
		return off
	else if bg_color.length isnt 7
		user_error_message = "Background Color needs to be in long hand format"
		$('#GridBgColor').focus()
		return off
	
	if parseFloat(transparency) is NaN
		user_error_message = "Transparency needs to be a floating point number"
		$('#GridTransparency').focus()
		return off
	
	unless 1 >= parseFloat(transparency) >= 0
		user_error_message = "Transparency need to be within the range of 1.0 and 0.0"
		$('#GridTransparency').focus()
		return off
	
	return on

generate_grid = (event)->
	event.preventDefault()
	if all_input_okay()
		[
			width
			height
			line_color
			bg_color
			transparency
		] = get_input_values()
		
		# it feels dirty touching a canvas using jquery
		canvas = document.getElementById "GridSurface"
		canvas.width = width
		canvas.height = height
		surface = canvas.getContext "2d"
		pixel_grid = new PixelGrid width, height, line_color, bg_color, transparency, surface
		pixel_grid.render()
		pixel_grid.get_image()
	else
		alert user_error_message

$ ->
	# Perform canvas check... if I can get a 2D context then we're okay
	$("#GridGenerator").on "submit", generate_grid
	$("#GenerateButton").removeAttr 'disabled'
	$("#InfoBox").html info_content.about
	$("input[type='number'], input[type='color']").each (index, element) ->
		$(element).focus =>
			switch @.id
				when 'GridWidth' then display_message = 'grid_width'
				when 'GridHeight' then display_message = 'grid_height'
				when 'GridLineColor' then display_message = 'grid_line_color'
				when 'GridBgColor' then display_message = 'grid_bg_color'
				when 'GridTransparency' then display_message = 'grid_transparency'
				else display_message = 'about'
			$('#InfoBox').html info_content[display_message]
		if element.id in ['GridLineColor', 'GridBgColor']
			$(element).click ->
				if element.id is 'GridLineColor'
					display_message = 'grid_line_color'
				else
					display_message = 'grid_bg_color'
				$('#InfoBox').html info_content[display_message]
		$(element).on "focusout", (event)->
			$('#InfoBox').html info_content.about
